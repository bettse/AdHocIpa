
xml.instruct! :xml, :version => '1.0', :encoding => 'UTF-8'
xml.declare! :DOCTYPE, :plist, :PUBLIC, '-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd'
xml.plist(:version => "1.0") {
  xml.dict {
    xml.key("items")
    xml.array {
      xml.dict {
        xml.key("assets")
        xml.array do
          xml.dict { |x| x.key("kind"); x.string("software-package"); x.key("url"); x.string("#{@ipa_url}")}
          xml.dict { |x| x.key("kind"); x.string("full-size-image"); x.key("needs-shine"); x.false; x.key("url"); x.string("#{@image_url}")}
          xml.dict { |x| x.key("kind"); x.string("display-image"); x.key("needs-shine"); x.false; x.key("url"); x.string("#{@icon_url}")}
        end
        xml.key("metadata")
        xml.dict { |x|
          x.key("bundle-identifier")
          x.string("#{@bundle_identifier}")
          x.key("bundle-version")
          x.string("#{@bundle_version}")
          x.key("kind")
          x.string("software")
          x.key("subtitle")
          x.string("<#{@title}")
          x.key("title")
          x.string("#{@subtitle}")
        }
      }
    }
  }
}
