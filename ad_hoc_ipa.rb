require 'ipa'
class AdHocIpa < Sinatra::Application
  configure :production do
    require 'rack/ssl-enforcer'
    use Rack::SslEnforcer

    set :haml, { ugly: true }
    set :clean_trace, true

    Dir.mkdir('logs') unless File.exist?('logs')

    $logger = Logger.new('logs/common.log','weekly')
    $logger.level = Logger::WARN

    # Spit stdout and stderr to a file during production
    # in case something goes wrong
    $stdout.reopen("logs/output.log", "w")
    $stdout.sync = true
    $stderr.reopen($stdout)
  end

  configure :development do
    $logger = Logger.new(STDOUT)
    register Sinatra::Reloader
  end

  helpers do
    def ipa_filename
      @ipa_filename ||= 'AppRTCDemo.ipa'
    end

    def ipa_path
      "public/#{ipa_filename}"
    end

    def base_url
      @base_url ||= "#{request.scheme}://#{request.env['HTTP_HOST']}"
    end

    def parse_ipa
      IPA::IPAFile.open(ipa_path) do |ipa|
        @title = ipa.name
        @subtitle = ipa.display_name
        @bundle_version = ipa.version
        @bundle_shortversion = ipa.version_string
        @bundle_identifier = ipa.identifier

      end
    end
  end

  before do
    parse_ipa
    @image_url = "#{base_url}/image.png" # png, size: 512x512
    @icon_url = "#{base_url}/icon.png"
  end

  get '/' do
    headers "Content-Type" => "text/html; charset=utf-8"
    @manifest_url = "itms-services://?action=download-manifest&url=#{base_url}/app.plist"
    haml :index
  end

  get '/app.plist' do
    content_type 'application/x-plist'
    @ipa_url = "#{base_url}/#{ipa_filename}"

    builder :plist
  end

  get '/image.png' do
    content_type 'image/png'
    IPA::IPAFile.open(ipa_path) do |ipa|
      response.write(ipa.payload_file('iTunesArtwork@2x'))
    end
  end

  get '/icon.png' do
    content_type 'image/png'
    IPA::IPAFile.open(ipa_path) do |ipa|
      response.write(ipa.payload_file('Icon@2x.png'))
    end
  end

end
