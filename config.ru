require 'rubygems'
require 'bundler'
Bundler.require(:default, ENV['RACK_ENV'].to_sym)  # only loads environment specific gems

require './ad_hoc_ipa'
run AdHocIpa.new
